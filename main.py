# Itential
# Last update: March 21 2018
try:
    import json, requests, io, os, time, sys, re, pymongo, logging, webbrowser
    from pprint import pprint
    from concurrent.futures import ThreadPoolExecutor, as_completed
    from allpairspy import AllPairs
    from prep import Prep
    from req import Req
    from writeReport import WriteReport
    from datetime import datetime
except ImportError:
    sys.exit("""Sorry I broke down :(, but I need the following dependencies: json, requests, io, os, time, sys, re, pymongo, logging, webbrowser, pprint, allpairspy, datetime.\
                if you are missing any of them, try to install them using "pip3 install packageName". Also make sure you are using python 3 and up.""")

if __name__ == "__main__":
    logging.basicConfig(filename='misc/logs.txt', level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
    print("Starting ...")
    # Load the settings file
    settingsData = ''
    try:
        settingsData = json.load(open('settings.json'))
    except:
        logging.info("I could not open the settings file :(")
        print("I could not open the settings file :(")    
    
    start_time = time.time()
    timeStart=datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    # Load the file
    appData=''
    try:
        appData = json.load(open(str(settingsData['mainApplication']['PronghonJSONPath'])))
    except:
        logging.info("I could not open the settings file :(")
        print("I could not open the settings file :(")
    
    if(appData):
        print("I read the file successfully! :D")
    else:
        print("I could not read the file :(")

    # Create an object to prep the pronghorn.json file
    prepDoc = Prep(appData, settingsData)

    # Create an object to issue API requests
    apiReq = Req(str(settingsData['mainApplication']['PronghornUsername']),str(settingsData['mainApplication']['PronghornPassword']), str(settingsData['mainApplication']['PronghornURL']), appData, settingsData)

    # Set the host and the application name
    hostUrl=str(settingsData['mainApplication']['PronghornURL'])
    applicationURL= str(appData["title"])

    # Prep the document for methods without input, GET methods, POST methods with input in URL, and POST methods with input in body
    prepDoc.prepNoInput()
    prepDoc.prepGET()
    prepDoc.prepPOSTURL()
    prepDoc.prepPOSTBody()

    # Get the prepared document
    prepObject = prepDoc.getMethodsDB()
    totalTests = 0
    for k in prepObject:
        totalTests = totalTests+len(prepObject[k]['calls'])+len(prepObject[k]['data'])
    print("I created "+str(totalTests)+" tests for methods get or method without input")

    # But first let me take a token ...
    token = apiReq.login()

    # Request methods without input, GET and POST methods with input in URL
    apiReq.reqNoInputAndGETPOSTURLSingle(prepObject, token)

    # Request POST methods with input in body
    apiReq.reqPOSTBodySingle(prepObject, token)

    timeEnd=datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    print("I finished all requests in --- %s seconds ---" % (time.time() - start_time))

    prepGlobal= prepDoc.getGlobals()
    reqGlobal= apiReq.getGlobals()
    reqGlobal['totalTests'] = totalTests

    wr = WriteReport(str(settingsData['mainApplication']['ExportReportFileName']))
    wr.addHead("Fuzzer Test Report - "+str(prepGlobal['application']))
    wr.addHeader(prepGlobal['application'], timeStart, timeEnd)
    wr.addStatus(prepGlobal, reqGlobal)
    wr.addTableStatus(prepDoc.getMethodsDB(), apiReq.getCallsDB())
    wr.addTOC(prepDoc.getMethodsDB())
    wr.addTableTests(prepDoc.getMethodsDB(), apiReq.getCallsDB())

    chrome_path = ''

    print('Done!')

    # if(str(settingsData['mainApplication']['Environment']).lower() == "mac"):
    #     # MacOS
    #     chrome_path = 'open -a /Applications/Google\ Chrome.app %s'
    # elif(str(settingsData['mainApplication']['Environment']).lower() == "windows"):
    #     # Windows
    #     chrome_path = 'C:\Program Files (x86)\Google\Chrome\Application\chrome.exe %s'
    # elif(str(settingsData['mainApplication']['Environment']).lower() == "linux"):
    #     # Linux
    #     chrome_path = '/usr/bin/google-chrome %s'

    # webbrowser.get(chrome_path).open(os.path.abspath("report/"+str(settingsData['mainApplication']['ExportReportFileName'])+".html"))

    # wr.generateJira(prepDoc.getMethodsDB(), apiReq.getCallsDB())