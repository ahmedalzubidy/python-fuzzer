# Itential
# Last update: March 21 2018
import json, requests, io, os, time, sys, re, pymongo, logging
from pprint import pprint
from concurrent.futures import ThreadPoolExecutor, as_completed
from allpairspy import AllPairs

class Prep:

    logging.basicConfig(filename='misc/logs.txt', level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

    def __init__(self, appData, settingsData):
        # self.arrayString = ['a','aa','null','undefined',' ', '!','@','$','%','^','&','(',')','위대문','위','a-a','a,a','%3F','?','|','%7C','-','+','1+1','-1', '0', '90071992547409923433333', '-90071992547409928888']
        # self.arrayArray = [['a'],['null'],['undefined'],[], ['!'],['@'],['$'],['%'],['^'],['&'],['(',')'],['위','대문'],['위'],[['a'],['a']],[{}],[{'%3F':'?'}], [90071992547409923433333, -90071992547409928888]]
        # self.arrayObject = [{},{'null':''},{'e':'undefined'},{'a':[1,2,3]}, {'a':'!'}, {'위':'대문'},{'a':'위'},{'a':{'a':['a']}},{'%3F':'?'}, {'a':90071992547409923433333, 'b':-90071992547409928888}]

        self.arrayString = settingsData['prepClass']['inputsForStringVariables']
        self.arrayArray = settingsData['prepClass']['inputsForArrayVariables']
        self.arrayObject = settingsData['prepClass']['inputsForObjectVariables']

        self.appData=appData
        self.methods=appData["methods"]
        print("I found " + str(len(self.methods)) + " methods in total")

        self.methodsInputGet=[]
        self.methodsInputPost=[]
        self.methodsNoInput=[]

        for m in self.methods:
            if(len(m["input"])>0 and m["route"] and "deprecated" not in m and m["route"]["verb"].lower()=="get"):
                self.methodsInputGet.append(m)
            elif(len(m["input"])>0 and m["route"] and "deprecated" not in m and m["route"]["verb"].lower()=="post"):
                self.methodsInputPost.append(m)
            elif(len(m["input"])<=0 and m["route"] and "deprecated" not in m):
                self.methodsNoInput.append(m)

        print(str(len(self.methodsInputGet)) + " methods with input and it is a get")
        print(str(len(self.methodsInputPost)) + " methods with input and it is a post")
        print(str(len(self.methodsNoInput)) + " methods without input")

        self.hostUrl=str(settingsData['mainApplication']['PronghornURL'])
        self.applicationURL= str(appData["title"])

        self.methodsPostUrl=[]
        self.methodsPostBody=[]
        self.methodsPostBodyUrl=[]
                
        for m in self.methodsInputPost:
            if(m["route"]["path"].count(":")==len(m["input"])):
                self.methodsPostUrl.append(m)
            else:
                if(":" in m["route"]["path"]):
                    self.methodsPostBodyUrl.append(m)
                else:
                    self.methodsPostBody.append(m)
        
        self.methodsDB={}

    def prepNoInput(self):
        for m in self.methodsNoInput:
            self.methodsDB[m['name']]={
                'calls': [],
                'type': str(m['route']['verb']).lower(),
                'route': str(m['route']['path']).lower(),
                'data': [],
            }
            u = self.hostUrl + self.applicationURL + m["route"]["path"]
            self.methodsDB[m['name']]['calls'].append(u)

    def prepGET(self):
        for m in self.methodsInputGet:
            self.methodsDB[m['name']]={
                'calls': [],
                'type': str(m['route']['verb']).lower(),
                'route': str(m['route']['path']).lower(),
                'data': [],
            }
            if(len(m["input"])>1):
                parameters=[]
                for j in range(0,len(m["input"])):
                    logging.info(j)
                    parameters.append(self.arrayString)
                for pairs in (AllPairs(parameters)):
                    pathArray=m["route"]["path"].split('/')
                    del pathArray[0]
                    paUrl= self.hostUrl+self.applicationURL
                    count=0
                    for pa in pathArray:
                        if(pa.startswith(":")):
                            paUrl=paUrl+"/"+pairs[count]
                            count=count+1
                        else:
                            paUrl=paUrl+"/"+pa
                    self.methodsDB[m['name']]['calls'].append(paUrl)
            else:
                for au in self.arrayString:
                    paUrl=self.applicationURL+m["route"]["path"]
                    temp1=paUrl[0:paUrl.index(":")]
                    temp2=paUrl[paUrl.index(":"):]
                    if "/" in temp2:
                        temp3 = temp2[paUrl.index("/"):]
                        paUrl = self.hostUrl+self.applicationURL+temp1+au+temp3
                        self.methodsDB[m['name']]['calls'].append(paUrl)
                    else:
                        paUrl = self.hostUrl+temp1+au
                        self.methodsDB[m['name']]['calls'].append(paUrl)

    def prepPOSTURL(self):
        for m in self.methodsPostUrl:
            self.methodsDB[m['name']]={
                'calls': [],
                'type': str(m['route']['verb']).lower(),
                'route': str(m['route']['path']).lower(),
                'data': [],
            }
            if(len(m["input"])>1):
                parameters=[]
                for j in range(0,len(m["input"])):
                    logging.info(j)
                    parameters.append(self.arrayString)
                for pairs in (AllPairs(parameters)):
                    pathArray=m["route"]["path"].split('/')
                    del pathArray[0]
                    paUrl= self.hostUrl+self.applicationURL
                    count=0
                    for pa in pathArray:
                        if(pa.startswith(":")):
                            paUrl=paUrl+"/"+pairs[count]
                            count=count+1
                        else:
                            paUrl=paUrl+"/"+pa
                    self.methodsDB[m['name']]['calls'].append(paUrl)
            else:
                for au in self.arrayString:
                    paUrl=self.applicationURL+m["route"]["path"]
                    temp1=paUrl[0:paUrl.index(":")]
                    temp2=paUrl[paUrl.index(":"):]
                    if "/" in temp2:
                        temp3 = temp2[paUrl.index("/"):]
                        paUrl = self.hostUrl+self.applicationURL+temp1+au+temp3
                        self.methodsDB[m['name']]['calls'].append(paUrl)
                    else:
                        paUrl = self.hostUrl+temp1+au
                        self.methodsDB[m['name']]['calls'].append(paUrl)
    
    def prepPOSTBody(self):
        for m in self.methodsPostBody:
            self.methodsDB[m['name']]={
                'calls': [],
                'type': str(m['route']['verb']).lower(),
                'route': str(m['route']['path']).lower(),
                'data': [],
            }
            if(len(m["input"])>1):
                mUrl=self.hostUrl+self.applicationURL+m["route"]["path"]
                self.methodsDB[m['name']]['calls'].append(mUrl)
                parameters=[]
                for mInput in m["input"]:   
                    if(mInput["type"].lower()=="string"):
                        parameters.append(self.arrayString)
                    if(mInput["type"].lower()=="array"):
                        parameters.append(self.arrayArray)
                    if(mInput["type"].lower()=="object"):
                        parameters.append(self.arrayObject)
                
                for pairs in (AllPairs(parameters)):
                    paData={}
                    count=0
                    for mInput in m["input"]:
                        mName=mInput["name"]
                        mType=mInput["type"]
                        paData[mName]=pairs[count]
                        count=count+1
                    self.methodsDB[m['name']]['data'].append(paData)
            else:
                mName=m["input"][0]["name"]
                mType=m["input"][0]["type"]
                mUrl=self.hostUrl+self.applicationURL+m["route"]["path"]
                self.methodsDB[m['name']]['calls'].append(mUrl)
                arrayUrlCurrent=[]

                if(mType.lower()=="string"):
                    arrayUrlCurrent=self.arrayString
                if(mType.lower()=="array"):
                    arrayUrlCurrent=self.arrayArray
                if(mType.lower()=="object"):
                    arrayUrlCurrent=self.arrayObject

                for au in arrayUrlCurrent:
                    paData={
                        mName:au
                    }
                    self.methodsDB[m['name']]['data'].append(paData)

    def getMethodsDB(self):
        return self.methodsDB

    def getGlobals(self):
        globalObject={
            'application':self.applicationURL,
            'totalMethods': str(len(self.methods)),
            'deprecatedMethods': str(len(self.methods)-(len(self.methodsNoInput)+len(self.methodsInputGet)+len(self.methodsInputPost))),
            'methodWithoutInput': str(len(self.methodsNoInput)),
            'methodGet': str(len(self.methodsInputGet)),
            'methodPost':str(len(self.methodsInputPost)),
            'methodPostInputURL':str(len(self.methodsPostUrl)),
            'methodPostInputBody':str(len(self.methodsPostBody)),
            'methodPostInputBodyURL':str(len(self.methodsPostBodyUrl))
        }
        return globalObject