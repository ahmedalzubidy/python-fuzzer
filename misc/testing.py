import json, requests
from pprint import pprint
from concurrent.futures import ThreadPoolExecutor, as_completed
from allpairspy import AllPairs
import re
import sys
import time
import pyopencl as cl
import pymongo
import urllib.parse

if __name__ == "__main__":
    # print("hi")
    # plat = cl.get_platforms()
    # devices = plat[0].get_devices()
    # ctx = cl.Context([devices[2]])
    # print(ctx.get_info(cl.context_info.DEVICES))

    # username = urllib.parse.quote_plus('user')
    # password = urllib.parse.quote_plus('pass/word')
    # MongoClient('mongodb://%s:%s@127.0.0.1' % (username, password))

    # a = np.arange(32).astype(np.float32)
    # res = np.empty_like(a)

    # plat = cl.get_platforms()
    # devices = plat[0].get_devices()
    # ctx = cl.Context([devices[2]])
    # queue = cl.CommandQueue(ctx)

    # mf = cl.mem_flags
    # a_buf = cl.Buffer(ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=a)
    # dest_buf = cl.Buffer(ctx, mf.WRITE_ONLY, res.nbytes)

    # client = pymongo.MongoClient('mongodb://localhost:27017/')

    # db =client['pronghorn']
    # collection = db['forms']

    # pprint(collection.find_one({}))

    # r2 = requests.get("http://localhost:3000/device_management/device_management/device/a"+"?token="+str("ZGVhNTliZjFiMzAwYjM0MWI0N2QwYTNjZDg5ZGI0NmE%3D"))

    # if("200" in str(r2) or "404" in str(r2) or "400" in str(r2)):
    #     print("hi")
    # else:
    #     print("hi2")

    methodsDB = {}
    methodsDB["ahmed"]={
        "name": [],
        "age": 3
    }
    methodsDB['ahmed']['name'].append('hi')

    pprint(methodsDB['ahmed']['name'])

    for m in methodsDB:
        print(m)



# Old code

    # def reqNoneAndGETMultithreading(self, apiCalls, token):
    #     def handleNoInputUrlInput(callIncoming, tokenIncoming):
    #         r2 = requests.get(callIncoming+"?token="+str(tokenIncoming))
    #         if("200" in str(r2) or "404" in str(r2) or "400" in str(r2)):
    #         # return(str(callIncoming[callIncoming.index("0/")+2:]) + " : All Good " + str(r2))
    #             return
    #         else:
    #             return(str(callIncoming[callIncoming.index("0/")+2:]) + " : " + str(r2))

    #     with ThreadPoolExecutor(max_workers = 3) as executor:
    #         futures = {executor.submit(handleNoInputUrlInput, call, token) for call in apiCalls}
    #         for f in as_completed(futures):
    #             pprint(f.result())
    #     print("I'm done with methods without input and GET methods with input in the URL \nI'm still running, and I did not forget about you, wait ...")

    # def reqNoneAndGETSingleThread(self, apiCalls, token):
    #     tokenIncoming=token
    #     for callIncoming in apiCalls:
    #         r2 = requests.get(callIncoming+"?token="+str(tokenIncoming))
    #         if("200" in str(r2) or "404" in str(r2) or "400" in str(r2)):
    #             print("None")
    #             r2Str=str(r2)
    #             self.methodsResponses.append('<td class="statusPass">PASS</td><td>'+r2Str[r2Str.find("["):]+'</td><td>'+str(callIncoming[callIncoming.index("0/")+2:])+'</td><td>None</td><td>None</td><td>None</td><td>None</td>')
    #             self.testPassedCount=self.testPassedCount+1
    #         else:
    #             self.testFailedCount=self.testFailedCount+1
    #             pprint(str(callIncoming[callIncoming.index("0/")+2:]) + " : " + str(r2))
    #             with open('/Users/aalzubidy/pioneer/server/logs/pronghorn.log', "rb") as f:
    #                 first = f.readline()
    #                 f.seek(-2, os.SEEK_END)
    #                 while f.read(1) != b"\n":
    #                     f.seek(-2, os.SEEK_CUR)
    #                 last = f.readline()
    #                 print(last)
    #                 last=str(last)
    #                 if "closed" in last or "crashed" in last:
    #                     self.testCrashedCount = self.testCrashedCount + 1
    #                 auditID=''
    #                 mongoDoc=''
    #                 if "audit_id" in last:
    #                     auditID=last[last.index('{"audit_id'):last.index(',"origin')]
    #                     auditID=auditID+"}"
                        
    #                     client = pymongo.MongoClient('mongodb://localhost:27017/')
    #                     db =client['pronghorn']
    #                     collection = db['audit_trail']
    #                     mongoDoc = collection.find_one(auditID)
    #                     pprint(mongoDoc)
    #             self.methodsResponses.append('<td class="statusFail">FAIL</td><td>'+str(r2)+'</td><td>'+str(callIncoming[callIncoming.index("0/")+2:])+'</td><td>None</td><td>'+str(last)+'</td><td>'+str(auditID)+'</td><td>'+str(mongoDoc)+'</td>')

    #     print("I'm done with methods without input and GET methods with input in the URL \nI'm still running, and I did not forget about you, wait ...")

    # def reqPostURLMultithreading(self, apiCallsPostUrl, token):
    #     def handlePostInputUrl(callIncoming, tokenIncoming):
    #         r3 = requests.post(callIncoming+"?token="+str(tokenIncoming))
    #         if("200" in str(r3) or "404" in str(r3) or "400" in str(r3)):
    #             # return(str(callIncoming[callIncoming.index("0/")+2:]) + " : All Good " + str(r2))
    #             return
    #         else:
    #             return(str(callIncoming[callIncoming.index("0/")+2:]) + " : " + str(r3))

    #     with ThreadPoolExecutor(max_workers = 3) as executor:
    #         futures = {executor.submit(handlePostInputUrl, call, token) for call in apiCallsPostUrl}

    #         for f in as_completed(futures):
    #             pprint(f.result())
    #     print("I'm done with POST methods with input in the URL only \nI'm still running, and I did not forget about you, wait ...")

    # def reqPostURLSingleThread(self, apiCallsPostUrl, token):
    #     tokenIncoming=token
    #     for callIncoming in apiCallsPostUrl:
    #         r3 = requests.post(callIncoming+"?token="+str(tokenIncoming))
    #         if("200" in str(r3) or "404" in str(r3) or "400" in str(r3)):
    #             # return(str(callIncoming[callIncoming.index("0/")+2:]) + " : All Good " + str(r2))
    #             print("None")
    #         else:
    #             pprint(str(callIncoming[callIncoming.index("0/")+2:]) + " : " + str(r3))
    #             with open('/Users/aalzubidy/pioneer/server/logs/pronghorn.log', "rb") as f:
    #                 first = f.readline()
    #                 f.seek(-2, os.SEEK_END)
    #                 while f.read(1) != b"\n":
    #                     f.seek(-2, os.SEEK_CUR)
    #                 last = f.readline()
    #                 print(last)
    #                 last=str(last)
    #                 if "audit_id" in last:
    #                     auditID=last[last.index('{"audit_id'):last.index(',"origin')]
    #                     auditID=auditID+"}"
                        
    #                     client = pymongo.MongoClient('mongodb://localhost:27017/')
    #                     db =client['pronghorn']
    #                     collection = db['audit_trail']
    #                     mongoDoc = collection.find_one(auditID)
    #                     pprint(mongoDoc)

    #     print("I'm done with POST methods with input in the URL only \nI'm still running, and I did not forget about you, wait ...")

    # def reqPostBodySingleThread(self, apiCallsPostBody, token):
    #     for call in apiCallsPostBody:
    #         callIncoming = call
    #         tokenIncoming= token
    #         arrayRes=[]
    #         for cd in apiCallsPostBody[callIncoming]:
    #             healthy="false"
    #             # secondsToSleep=10
    #             while(healthy=="false"):
    #                 rCheckHealth = requests.get(self.hostUrl+"health/module/"+self.appData["export"]+"?token="+str(tokenIncoming))
    #                 nooC=rCheckHealth.text.count("RUNNING")
    #                 if(str(nooC)=="1"):
    #                     healthy="true"
    #                 else:
    #                     time.sleep(3)
    #                 # time.sleep(secondsToSleep)
    #                 # secondsToSleep=secondsToSleep+10

    #             r4 = requests.post(callIncoming+"?token="+str(tokenIncoming), json = cd)
    #             if("200" in str(r4) or "404" in str(r4) or "400" in str(r4)):
    #                 # return(str(callIncoming[callIncoming.index("0/")+2:]) + " : All Good " + str(r2))
    #                 # arrayRes.append("none")
    #                 pass
    #             else:
    #                 # arrayRes.append(str(callIncoming[callIncoming.index("0/")+2:]) + " - Data: " + str(cd) + " : " + str(r4))
    #                 arrayRes.append(str(callIncoming[callIncoming.index("0/")+2:]) + " : " + str(r4))
    #                 print(str(callIncoming[callIncoming.index("0/")+2:]) + " : " + str(r4) + " --- Data: "+str(cd))
    #                 with open('/Users/aalzubidy/pioneer/server/logs/pronghorn.log', "rb") as f:
    #                     first = f.readline()
    #                     f.seek(-2, os.SEEK_END)
    #                     while f.read(1) != b"\n":
    #                         f.seek(-2, os.SEEK_CUR)
    #                     last = f.readline()
    #                     print(last)
    #                     last=str(last)
    #                     if "audit_id" in last:
    #                         auditID=last[last.index('{"audit_id'):last.index(',"origin')]
    #                         auditID=auditID+"}"
                            
    #                         client = pymongo.MongoClient('mongodb://localhost:27017/')
    #                         db =client['pronghorn']
    #                         collection = db['audit_trail']
    #                         mongoDoc = collection.find_one(auditID)
    #                         pprint(mongoDoc)
                            
    #         # return arrayRes
    #     print("I'm done with POST methods with input in the body only \nI'm still running, and I did not forget about you, wait ...")
