# Itential
# Last update: March 21 2018
import json, requests, io, os, time, sys, re, pymongo, logging
from pprint import pprint
from concurrent.futures import ThreadPoolExecutor, as_completed
from allpairspy import AllPairs

class Req:

    logging.basicConfig(filename='logs.txt', level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

    def __init__(self, username, password, serverLink, appData, settingsData):
        self.username=username
        self.password=password
        self.serverLink = serverLink
        self.token=''
        self.hostUrl=serverLink
        self.applicationURL= str(appData["title"])
        self.appData=appData
        self.testPassedCount=0
        self.testFailedCount=0
        self.testCrashedCount=0
        self.methodsResponses=[]
        self.callsDB={}
        self.settingsData=settingsData

    def login(self):
        # But first let me take a token ... 
        print('I am getting a token ...')
        loginLink = str(self.serverLink) + "/login"

        loginData = {"user":{
            "username": self.username,
            "password": self.password
        }}
        
        r = requests.post(url = loginLink, json = loginData)

        self.token = r.text
        print("I got the token: %s"%self.token)
        return self.token
    
    def reqNoInputAndGETPOSTURLSingle(self, apiCalls, token):
        for m in apiCalls:
            for callIncoming in apiCalls[m]["calls"]:
                tokenIncoming=token
                data = ''
                status = ''
                auditID = ''
                mongoDoc=''
                last=''
                if(apiCalls[m]['type'].lower()=="post"):
                    r2 = requests.post(callIncoming+"?token="+str(tokenIncoming))
                else:
                    r2 = requests.get(callIncoming+"?token="+str(tokenIncoming), verify=False)
                r2Str=str(r2)
                r2str=r2Str[r2Str.find("["):r2Str.find(">")]
                if("200" in str(r2) or "404" in str(r2) or "400" in str(r2)):
                    self.testPassedCount=self.testPassedCount+1
                    status="passed"
                else:
                    self.testFailedCount=self.testFailedCount+1
                    status="failed"
                    with open(str(self.settingsData['mainApplication']['pronghornLogsPath']), "rb") as f:
                        first = f.readline()
                        logging.info(first)
                        f.seek(-2, os.SEEK_END)
                        while f.read(1) != b"\n":
                            f.seek(-2, os.SEEK_CUR)
                        last = f.readline()
                        last=str(last)
                        if "closed" in last or "crashed" in last:
                            self.testCrashedCount = self.testCrashedCount + 1
                            status="crashed"
                        if "audit_id" in last:
                            auditID=last[last.index('{"audit_id'):last.index(',"origin')]
                            auditID=auditID+"}"
                            
                            client = pymongo.MongoClient(str(self.settingsData['reqClass']['pronghornMongoURL']))
                            db =client['pronghorn']
                            collection = db['audit_trail']
                            mongoDoc = collection.find_one(auditID)
                
                r2Parsed=''

                if "message" in str(r2.text):
                    r2Parsed = str(r2.text)[0:str(r2.text).index('","')]
                else:
                    r2Parsed=r2.text

                lastParsed=''
                if "message" in str(last):
                    lastParsed = str(last)[0:str(last).index('","')]
                else:
                    lastParsed=last

                callDataObj={
                    'name': str(m),
                    'data': data,
                    'status': status,
                    'responseCode': r2str,
                    'response': str(r2Parsed),
                    'log': lastParsed,
                    'auditID': auditID,
                    'auditTrail': mongoDoc
                }
                self.callsDB[callIncoming]=callDataObj
                pprint(callDataObj)

        print("I'm done with methods without input and GET and POST methods with input in the URL \nI'm still running, and I did not forget about you, wait ...")

    def reqPOSTBodySingle(self, apiCallsPostBody, token):
        nameCount=0
        for m in apiCallsPostBody:
            tokenIncoming=token
            for cd in apiCallsPostBody[m]['data']:
                status = ''
                auditID = ''
                mongoDoc=''
                last=''
                nameCount=nameCount+1
                healthy="false"

                while(healthy=="false"):
                    rCheckHealth = requests.get(self.hostUrl+"/health/module/"+self.appData["export"]+"?token="+str(tokenIncoming))
                    nooC=rCheckHealth.text.count("RUNNING")
                    if(str(nooC)=="1"):
                        healthy="true"
                    else:
                        time.sleep(3)
                    
                r4 = requests.post(apiCallsPostBody[m]['calls'][0]+"?token="+str(tokenIncoming), json = cd)
                r4Str=str(r4)
                r4str=r4Str[r4Str.find("["):r4Str.find(">")]
                if("200" in str(r4) or "404" in str(r4) or "400" in str(r4)):
                    self.testPassedCount=self.testPassedCount+1
                    status="passed"
                else:
                    self.testFailedCount=self.testFailedCount+1
                    status="failed"
                    with open(str(self.settingsData['mainApplication']['pronghornLogsPath']), "rb") as f:
                        first = f.readline()
                        logging.info(first)
                        f.seek(-2, os.SEEK_END)
                        while f.read(1) != b"\n":
                            f.seek(-2, os.SEEK_CUR)
                        last = f.readline()
                        last=str(last)
                        
                        r2Parsed=''

                        if "message" in str(r4.text):
                            r2Parsed = str(r4.text)[0:str(r4.text).index('","')]
                        else:
                            r2Parsed=r4.text

                        if "closed" in last or "crashed" in last or "closed" in str(r2Parsed) or "crashed" in str(r2Parsed):
                            self.testCrashedCount = self.testCrashedCount + 1
                            status="crashed"
                        if "audit_id" in last:
                            auditID=last[last.index('{"audit_id'):last.index(',"origin')]
                            auditID=auditID+"}"
                            
                            client = pymongo.MongoClient(str(self.settingsData['reqClass']['pronghornMongoURL']))
                            db =client['pronghorn']
                            collection = db['audit_trail']
                            mongoDoc = collection.find_one(auditID)
                
                r2Parsed=''

                if "message" in str(r4.text):
                    r2Parsed = str(r4.text)[0:str(r4.text).index('","')]
                else:
                    r2Parsed=r4.text

                lastParsed=''
                if "message" in str(last):
                    lastParsed = str(last)[0:str(last).index('","')]
                else:
                    lastParsed=last
                
                callDataObj={
                    'name': str(m),
                    'data': cd,
                    'status': status,
                    'responseCode': r4str,
                    'response': str(r2Parsed),
                    'log': lastParsed,
                    'auditID': auditID,
                    'auditTrail': mongoDoc
                }
                self.callsDB[apiCallsPostBody[m]['calls'][0]+"-"+str(nameCount)]=callDataObj
                pprint(callDataObj)
                            
        print("I'm done with POST methods with input in the body only \nI'm still running, and I did not forget about you, wait ...")

    def getCallsDB(self):
        return self.callsDB

    def getGlobals(self):
        globalObject={
            'testPassed': str(self.testPassedCount),
            'testFailed': str(self.testFailedCount),
            'testCrashed': str(self.testCrashedCount)
        }
        return globalObject