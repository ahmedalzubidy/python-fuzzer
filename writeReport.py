# Itential
# Last update: March 21 2018
import json, requests, io, os, time, sys, re, pymongo, logging
from pprint import pprint
from concurrent.futures import ThreadPoolExecutor, as_completed
from allpairspy import AllPairs

class WriteReport:

    logging.basicConfig(filename='misc/logs.txt', level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

    def __init__(self, fileName):
        self.fileName = fileName
        print('Setting up a report')
        fh = open("report/"+str(self.fileName)+".html", "w")
        fh.close
    
    def addHead(self, title):
        # Put the head for the report
        fh = open("report/"+str(self.fileName)+".html", "a")
        fh.write('<!DOCTYPE html>\
        <html lang="en">\
        <head>\
        <meta charset="UTF-8">\
        <title>' + str(title) + '</title>\
        <link rel="stylesheet" type="text/css" href="css/index.css">\
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">\
        </head>\
        <body>')
        fh.close
        print('I added head')
    
    # <link href="https://fonts.googleapis.com/css?family=Roboto:300i,400" rel="stylesheet">\
    # <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">\

    def addHeader(self, applicationName, timeStart, timeEnd):
        # Put the header for the report
        fh = open("report/"+str(self.fileName)+".html", "a")
        fh.write('<div class="header"><h3>Title: '+applicationName+'</h3>\
        <h3>Start Time: '+timeStart+'</h3>\
        <h3>End Time: '+timeEnd+'</h3></div><br>\
        ')
        fh.close
        print('I added header')

    def addStatus(self, prepObject, reqObject):
        # Put the status for the report
        fh = open("report/"+str(self.fileName)+".html", "a")
        fh.write('<div class="status"><h4>Total methods: '+prepObject['totalMethods']+'</h4>\
        <h4>Deprecated methods: '+prepObject['deprecatedMethods']+'</h4>\
        <h4>Methods without input: '+prepObject['methodWithoutInput']+'</h4>\
        <h4>Methods GET: '+prepObject['methodGet']+'</h4>\
        <h4>Methods POST: '+prepObject['methodPost']+'</h4>\
        <h4>Methods POST - input in URL: '+prepObject['methodPostInputURL']+'</h4>\
        <h4>Methods POST - input in body: '+prepObject['methodPostInputBody']+'</h4>\
        <h4>Methods POST - input URL and Body (not tested): '+prepObject['methodPostInputBodyURL']+'</h4>\
        <h4>Total tests: '+str(reqObject['totalTests'])+'</h4>\
        <h4>Tests passed: '+str(reqObject['testPassed'])+'</h4>\
        <h4>Tests failed: '+str(reqObject['testFailed'])+'</h4>\
        <h4>Tests crashed: '+str(reqObject['testCrashed'])+'</h4></div><br>\
        ')
        fh.close
        print('I added status')
    
    def addTableStatus(self, methodsDB, callsDB):
        # Put the header for the table
        fh = open("report/"+str(self.fileName)+".html", "a")
        fh.write('\
        <a name="tableStatusLink"><h3>Methods Summary</h3></a>\
        <table id="" class="display" style="width:100%">\
        <thead>\
        <tr>\
        <th>Method</th>\
        <th>Total Tests</th>\
        <th>Passed Tests</th>\
        <th>Failed Tests</th>\
        <th>Crashed Tests</th>\
        </tr>\
        </thead>\
        <tbody>')
        for m in methodsDB:
            totalTests= len(methodsDB[m]['calls'])+len(methodsDB[m]['data'])
            testsPassed = 0
            testsFailed = 0
            testsCrashed = 0

            for m2 in callsDB:
                if m == callsDB[m2]['name']:
                    if callsDB[m2]['status']=="passed":
                        testsPassed=testsPassed+1
                    if callsDB[m2]['status']=="failed":
                        testsFailed=testsFailed+1
                    if callsDB[m2]['status']=="crashed":
                        testsCrashed=testsCrashed+1
            fh.write('<tr>')
            fh.write('<td><a href="#'+m+'Link">'+str(m)+' ('+str(methodsDB[m]['type'])+')'+'</a></td>')
            fh.write('<td>'+str(totalTests)+'</td>')
            fh.write('<td>'+str(testsPassed)+'</td>')
            fh.write('<td>'+str(testsFailed)+'</td>')
            if(testsCrashed>=1):
                fh.write('<td class="statusCrashed">'+str(testsCrashed)+'</td>')
            else:
                fh.write('<td>'+str(testsCrashed)+'</td>')
            fh.write('</tr>')
        fh.write('</tbody></table>')
        fh.close
        print('I added status')

    def addTableTests(self, methodsDB, callsDB):
        # Put the header for the table
        fh = open("report/"+str(self.fileName)+".html", "a")
        
        for m in methodsDB:
            # fh.write('<button class="accordion">'+m+'</button>')
            # fh.write('<div class="panel"><br>')
            fh.write('<br><a name="'+m+'Link"></a><h3>'+m+' ('+str(methodsDB[m]['type'])+') <a href="#tableStatusLink">&uarr;</a></h3>')
            passedTests={}
            failedTests={}
            crashedTests={}

            for m2 in callsDB:
                if m == callsDB[m2]['name']:
                    if callsDB[m2]['status'] == 'passed':
                        passedTests[m2]=callsDB[m2]
                    if callsDB[m2]['status'] == 'failed':
                        failedTests[m2]=callsDB[m2]
                    if callsDB[m2]['status'] == 'crashed':
                        crashedTests[m2]=callsDB[m2]
            if(1==1):
                # fh.write('<h4>Passed</h4>')
                fh.write('<table id="" class="display" style="width:100%">\
                <thead>\
                <tr>\
                <th>ID</th>\
                <th>Status</th>\
                <th>Call</th>\
                <th>Response Code</th>\
                <th>Input</th>\
                <th>Response</th>\
                <th>Log Error</th>\
                <th>Audit ID</th>\
                <th>Audit Trail</th>\
                </tr>\
                </thead>\
                <tbody>')

                index = 0
                for m2 in passedTests:
                    index=index+1
                    fh.write('<tr>\
                    <td>'+str(index)+'</td>\
                    <td class="statusPassed">'+str(passedTests[m2]['status'])+'</td>\
                    <td>'+str(m2)+'</td>\
                    <td>'+str(passedTests[m2]['responseCode'])+'</td>\
                    <td>'+str(passedTests[m2]['data'])+'</td>\
                    <td>'+str(passedTests[m2]['response'])+'</td>\
                    <td>'+str(passedTests[m2]['log'])+'</td>\
                    <td>'+str(passedTests[m2]['auditID'])+'</td>\
                    <td>'+str(passedTests[m2]['auditTrail'])+'</td>\
                    </tr>')

            #     fh.write('</tbody></table>')

            # if(len(failedTests)>=1):
            #     fh.write('<h4>Failed</h4>')
            #     fh.write('<table id="" class="display" style="width:100%">\
            #     <thead>\
            #     <tr>\
            #     <th>ID</th>\
            #     <th>Status</th>\
            #     <th>Call</th>\
            #     <th>Response Code</th>\
            #     <th>Input</th>\
            #     <th>Response</th>\
            #     <th>Log Error</th>\
            #     <th>Audit ID</th>\
            #     <th>Audit Trail</th>\
            #     </tr>\
            #     </thead>\
            #     <tbody>')

            #     index = 0
                for m2 in failedTests:
                    index=index+1
                    fh.write('<tr>\
                    <td>'+str(index)+'</td>\
                    <td class="statusFailed">'+str(failedTests[m2]['status'])+'</td>\
                    <td>'+str(m2)+'</td>\
                    <td>'+str(failedTests[m2]['responseCode'])+'</td>\
                    <td>'+str(failedTests[m2]['data'])+'</td>\
                    <td>'+str(failedTests[m2]['response'])+'</td>\
                    <td>'+str(failedTests[m2]['log'])+'</td>\
                    <td>'+str(failedTests[m2]['auditID'])+'</td>\
                    <td>'+str(failedTests[m2]['auditTrail'])+'</td>\
                    </tr>')

            #     fh.write('</tbody></table>')

            # if(len(crashedTests)>=1):
            #     fh.write('<h4>Crashed</h4>')
            #     fh.write('<table id="" class="display" style="width:100%">\
            #     <thead>\
            #     <tr>\
            #     <th>ID</th>\
            #     <th>Status</th>\
            #     <th>Call</th>\
            #     <th>Response Code</th>\
            #     <th>Input</th>\
            #     <th>Response</th>\
            #     <th>Log Error</th>\
            #     <th>Audit ID</th>\
            #     <th>Audit Trail</th>\
            #     </tr>\
            #     </thead>\
            #     <tbody>')
            
            #     index = 0
                for m2 in crashedTests:
                    index=index+1
                    fh.write('<tr>\
                    <td>'+str(index)+'</td>\
                    <td class="statusCrashed">'+str(crashedTests[m2]['status'])+'</td>\
                    <td>'+str(m2)+'</td>\
                    <td>'+str(crashedTests[m2]['responseCode'])+'</td>\
                    <td>'+str(crashedTests[m2]['data'])+'</td>\
                    <td>'+str(crashedTests[m2]['response'])+'</td>\
                    <td>'+str(crashedTests[m2]['log'])+'</td>\
                    <td>'+str(crashedTests[m2]['auditID'])+'</td>\
                    <td>'+str(crashedTests[m2]['auditTrail'])+'</td>\
                    </tr>')
            
                fh.write('</tbody></table>')

        fh.write('<script src="https://code.jquery.com/jquery-1.12.4.js"></script>')
        # fh.write('<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>')
        fh.write('<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>')
        fh.write('<script src="js/index.js"></script></body></html>')
        fh.close
        print('I added test')

    def addTOC(self, methodsDB):
        # Put the table of contents
        fh = open("report/"+str(self.fileName)+".html", "a")
        fh.write('<br><ul>')
        for m in methodsDB:
            fh.write('<li><a href="#'+m+'Link">'+m+' ('+str(methodsDB[m]['type'])+')</a></li>')
        fh.write('</ul><br>')
        fh.close
        print('I added test')

    def generateJira(self, methodsDB, callsDB):
        fh = open("report/JIRATickets.txt", "w")
        fh.close

        fh = open("report/JIRATickets.txt", "a")
        
        callsDBCrashed={}

        for call in callsDB:
            if callsDB[call]['status'] == "crashed":
                if not callsDBCrashed[callsDB[call]['name']]:
                    callsDBCrashed[callsDB[call]['name']]=[]

                callsDBCrashed[callsDB[call]['name']].append(call=callsDB[call])
        
        pprint(callsDBCrashed)

        # for call in callsDBCrashed:
        #     fh.write('Project: Internal Pronghorn Support Orchestrator (IPSO)\n\
        #     Issue Type: Bug\n\
        #     Summary: '+call+' Crashed using Fuzzer\n')
        #     fh.write('Description: '+call+' crashed while running fuzzer. It fails on the following calls\n\
        #     '+callsDBCrashed[call]+'\
        #     Steps to Reproduce: # Run Postman # Call '+callsDBCrashed[call]['call']+' \n\
        #     Actual Result: # Application Crash # Logs: \n\
        #     Expected Result: # Application return an error but does not crash\n\
        #     Component: \n\
        #     Customer: Please select a customer\n\
        #     PronghornVersions: Attach your Pronghorn BluePrint\n\
        #     SystemType: Development\n\
        #     Priority: Low\n')
        
        fh.close